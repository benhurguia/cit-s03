package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	
	public void init() throws ServletException {
		System.out.println("****************************************");
		System.out.println(" CalculatorServlet has been initialized");
		System.out.println("****************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app</h1>"
				+ "<p>To use the app, input two numbers and an operation.</p>"
				+ "<p>Hit the submit button after filling in the details.</p>"
				+ "<p>You will get the result shown in your browser!</p>");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		float num1 = Float.parseFloat(req.getParameter("num1"));
		float num2 = Float.parseFloat(req.getParameter("num2"));
		String usedOperation = req.getParameter("operation");
		float result = 0;
		
		if(usedOperation.equals("add"))
			result = num1 + num2;
		else if(usedOperation.equals("subtract"))
			result = num1 - num2;
		else if(usedOperation.equals("multiply"))
			result = num1 * num2;
		else if(usedOperation.equals("divide"))
			result = num1 / num2;
			
		PrintWriter out = res.getWriter();
		out.println("<p>The two numbers you provided are: ");
		out.format("%.2f, %.2f", num1, num2);
		out.println("<p>The operation that you wanted is: " + usedOperation + "</p>");
		out.println("<p>The result is:");
		out.format("%.2f", result);
	}
	
	public void destroy() {
		System.out.println("****************************************");
		System.out.println(" CalculatorServlet has been destroyed");
		System.out.println("****************************************");
	}

}
